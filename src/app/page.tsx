import styles from "./page.module.css";

export default function Home() {
  return (
    <main className='main'>
      <div className='container'>
        <section>
          <h1>inKONBINI</h1>
          <p>
            Step into the shoes of Makoto Hayakawa to spend a week behind the
            counter at an authentic small-town convenience store.
          </p>
          <p>
            Discover joy and wonder hidden behind the daily routine, engage in
            meaningful conversations with your customers, and see how their
            stories intertwine in a beautifully-rendered early 1990s Japan.
          </p>
        </section>
        <section>
          <h2>WATCH THE ANNOUNCEMENT TEASER</h2>
          <p>WISHLIST THE GAME</p>
          <p>
            Mundane and extraordinary… In our lives, can we really tell them
            apart?..
          </p>
        </section>
        <section>
          <h2>ABOUT</h2>
          <p>
            inKONBINI: One Store, Many Stories is an upcoming narrative-driven
            simulation game that offers a meditative experience centered around
            a small-town convenience store and the lives of its customers.
          </p>
          <p>
            Focusing on day-to-day adventures of a convenience store clerk, it’s
            an accessible and relaxing game tailored to the tastes of players
            who find pleasure in light-hearted stories and dialogue interaction
            with funny and relatable characters.
          </p>
        </section>
        <section>
          <h2>KEY FEATURES</h2>
          <div className={styles["features-grid"]}>
            <article>
              <h3>THE SPACE FILLED WITH STORIES</h3>
              <p>
                Take your time to explore the store, pick up clues from your
                interaction with the environment, and piece together a complete
                picture of the overall story through multiple narrative threads.
              </p>
            </article>
            <article>
              <h3>YOUR CHOICES MATTER</h3>
              <p>
                Study your customers’ purchasing habits and use your knowledge
                of the store’s assortment to surprise them with new flavor
                options. Change their ordinary routine in choice-driven
                dialogues and see how you can impact their lives.
              </p>
            </article>
            <article>
              <h3>DEEP AND ACCESSIBLE MECHANICS</h3>
              <p>
                Immerse yourself in a calm and relaxing flow of a konbini
                worker’s routine. Find your rhythm in mundane activities like
                shelving and cleaning to discover new meaning in ordinary
                things.
              </p>
            </article>
          </div>
        </section>
        <section>
          <h2>MEDIA</h2>
          <p>
            Take your first look at what inKONBINI has to offer with a
            collection of in-game screenshots!
          </p>
        </section>
        <section>
          <h2>inKONBINI: One Store, Many Stories</h2>
          <p>
            Is currently in development at Nagai Industries for PC, Mac, and
            home consoles.
          </p>
          <p>Wishlist the game on Steam and follow our social channels!</p>
        </section>
        <section>
          <h2>ABOUT THE STUDIO</h2>
          <p>
            Nagai Industries is a compact, diverse team of highly-skilled video
            game creators focused on delivering accessible, high-quality
            interactive experiences with strong narrative elements.
          </p>
        </section>
      </div>
    </main>
  );
}

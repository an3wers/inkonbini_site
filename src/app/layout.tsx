import type { Metadata } from "next";
import "./styles/globals.css";

export const metadata: Metadata = {
  title: "inKONBINI: One Store, Many Stories",
  description:
    "inKONBINI: One Store, Many Stories is an upcoming narrative-driven simulation game that offers a meditative experience centered around a small-town convenience store and the lives of its customers.",
  keywords: "simulation game, meditative game, PC, Mac, home consoles",
  openGraph: {
    title: "inKONBINI: One Store, Many Stories",
    url: "https://inkonbini.com",
    description:
      "inKONBINI: One Store, Many Stories is an upcoming narrative-driven simulation game that offers a meditative experience centered around a small-town convenience store and the lives of its customers.",
  },
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang='en'>
      <body>{children}</body>
    </html>
  );
}
